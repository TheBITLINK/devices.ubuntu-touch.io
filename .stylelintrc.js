module.exports = {
  extends: ["stylelint-config-standard-scss", "stylelint-prettier/recommended"],
  rules: {
    "font-family-name-quotes": "always-unless-keyword",
    "alpha-value-notation": "number",
    "scss/no-global-function-names": null,
    "color-function-notation": null
  }
};
