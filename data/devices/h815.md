---
name: "LG G4"
deviceType: "phone"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "-"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
      - id: "photo"
        value: "+"
      - id: "video"
        value: "-"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "-"
      - id: "dataConnection"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "-"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "-"
      - id: "factoryReset"
        value: "-"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "-"
      - id: "wirelessExternalMonitor"
        value: "-"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+-"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "?"
        bugTracker: "https://github.com/ubports/ubuntu-touch/issues/245"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "-"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "?"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "+-"
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "chipset"
    value: "Qualcomm MSM8992 Snapdragon 808"
  - id: "gpu"
    value: "Adreno 418"
  - id: "rom"
    value: "32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 5.1.1 to 7.0"
  - id: "battery"
    value: "3000 mAh"
  - id: "display"
    value: "1440x2560 pixels, 5.5 in"
  - id: "rearCamera"
    value: "16MP"
  - id: "frontCamera"
    value: "8MP"
contributors:
  - name: Ari Kröyer
    photo:
    forum:
externalLinks:
  - name: "Kernel source"
    link: "https://github.com/abkro/android_kernel_lge_msm8992/"
    icon: "yumi"
  - name: "Device source"
    link: "https://github.com/abkro/android_device_lge_h815/"
    icon: "github"
  - name: "Device source - common"
    link: "https://github.com/abkro/android_device_lge_g4-common/"
    icon: "github"
---
