---
name: "Samsung Galaxy J1 (2016)"
deviceType: "phone"
portType: "Halium 10.0"
#[description: <marketing-style description that helps a user decide if this device is right for them>]
installLink: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte"
#[buyLink: <link to manufacturer's store, if it is available>]
disableBuyLink: true
#[price: <average price of the device in dollars>]
#[subforum: <the subforum id on forums.ubports.com, should be added in the format number/my-device>]
#[tag: <promoted|unmaintained>]
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "x"
      - id: "torchlight"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/1"
      - id: "vibration"
        value: "+"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/1"
      - id: "photo"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/1"
      - id: "video"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/1"
      - id: "switchCamera"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/1"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "x"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/16"
      - id: "pinUnlock"
        value: "?"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "?"
      - id: "noRebootTest"
        value: "?"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "?"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "-"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/8"
      - id: "onlineCharging"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/8"
      - id: "recoveryImage"
        value: "?"
      - id: "factoryReset"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/4"
      - id: "rtcTime"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/17"
        value: "+-"
      - id: "sdCard"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/4"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "?"
      - id: "waydroid"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/12"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/5"
      - id: "flightMode"
        value: "+"
      - id: "fmRadio"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/6"
      - id: "hotspot"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/3"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/7"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/9"
      - id: "fingerprint"
        value: "x"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/9"
      - id: "rotation"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/9"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/15"
      - id: "loudspeaker"
        value: "+-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/15"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/11"
      - id: "adb"
        value: "-"
        bugTracker: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte/issues/11"
      - id: "wiredExternalMonitor"
        value: "?"
deviceInfo:
  - id: "cpu"
    value: "Quad-core 1.3 GHz Cortex-A7"
  - id: "chipset"
    value: "Samsung Exynos 3 Quad 3475"
  - id: "gpu"
    value: "Mali-T720"
  - id: "ram"
    value: "1 GB"
  - id: "rom"
    value: "8 GB"
  - id: "android"
    value: "Android 5, officially upgradeable to 6"
  - id: "battery"
    value: "2050 mAh, replaceable"
  - id: "display"
    value: '4.5", 800x480'
  - id: "rearCamera"
    value: "5 MP"
  - id: "frontCamera"
    value: "2 MP"
  - id: "arch"
    value: "ARM"
  - id: "dimensions"
    value: '5.22" x 2.73" x 0.35"'
  - id: "weight"
    value: "4.62 oz"
  - id: "releaseDate"
    value: "Febuary 1st, 2016"
contributors:
  - name: "j1xltegtelwifiue"
    forum: "https://forums.ubports.com/user/j1xltegtelwifiue"
    photo:
communityHelp: # Links for the user to get help during installation and usage
  - name: "UBports Welcome & Install"
    link: "https://t.me/WelcomePlus"
    icon: "telegram" # Available icons can be found in the static/img/ folder
externalLinks: # Development resources
  - name: "Instructions, issues, and contributions"
    link: "https://github.com/j1xlte-gtelwifiue/android_device_samsung_j1xlte"
    icon: "github" # Available icons can be found in the static/img/ folder
---

### Contributors

[@j1xlte-gtelwifiue](https://github.com/j1xlte-gtelwifiue)

Thank you to the following contributors who worked on the [unofficial LineageOS version](https://forum.xda-developers.com/t/rom-10-0-0_r41-beta-lineageos-17-1-for-samsung-galaxy-j1-2016-exynos-3475.4307593/) this Ubuntu Touch port is based off of

- [@lzzy12](https://forum.xda-developers.com/m/8152187/)
- [@TBM 13](https://forum.xda-developers.com/m/9120939/)
- [@bengris32](https://forum.xda-developers.com/m/11364767/)
- [@imranpopz](https://forum.xda-developers.com/m/8241792/)
- [@cıyanogen](https://forum.xda-developers.com/m/7799844/)
- [@deadman96385](https://forum.xda-developers.com/m/4222965/)
- [@FieryFlames](https://forum.xda-developers.com/m/11495171/)

Thanks also to everyone who has contributions on Exynos3475 Nougat.

Special thanks to

- [@ananjaser1211](https://forum.xda-developers.com/m/4637718/)
- [@Stricted](https://forum.xda-developers.com/m/8184192/)
- [@danwood76](https://forum.xda-developers.com/m/6707196/)

and all of the 5433/7580 AOSP developers/contributors.

**_You must include this contributor list in any projects created from these sources._**
