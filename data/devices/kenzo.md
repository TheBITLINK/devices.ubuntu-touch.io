---
name: "Xiaomi Redmi Note 3 (kenzo)"
deviceType: "phone"
portType: "Halium 9.0"
description: "Released in November 2015, the Xiaomi Redmi Note 3 aka kenzo is one of the best value for money mid-ranger of its time with a massive  4000mAh battery , it also had a 5.5 inch IPS LCD display, Hexa-core Snapdragon 650 processor with 2 big cores for perfomence and 4 little cores for efficiency , and comes with either 2GB of RAM and 16GB internal storage, or 3GB of RAM with 32GB internal storage. The main shooter is 16MP with an f/2.2 aperture, while the front camera is 5MP with f/2.0 aperture."
price: 200
subforum: "103/xiaomi-redmi-note-3"
portStatus:
  - categoryName: "Actors"
    features:
      - id: "manualBrightness"
        value: "+"
      - id: "notificationLed"
        value: "+"
      - id: "torchlight"
        value: "+"
      - id: "vibration"
        value: "-"
  - categoryName: "Camera"
    features:
      - id: "flashlight"
        value: "+"
      - id: "photo"
        value: "+-"
      - id: "video"
        value: "+-"
      - id: "switchCamera"
        value: "+"
  - categoryName: "Cellular"
    features:
      - id: "carrierInfo"
        value: "+"
      - id: "dataConnection"
        value: "+"
      - id: "dualSim"
        value: "+"
      - id: "calls"
        value: "+"
      - id: "mms"
        value: "+"
      - id: "pinUnlock"
        value: "+"
      - id: "sms"
        value: "+"
      - id: "audioRoutings"
        value: "+"
      - id: "voiceCall"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "Endurance"
    features:
      - id: "batteryLifetimeTest"
        value: "+"
      - id: "noRebootTest"
        value: "+"
  - categoryName: "GPU"
    features:
      - id: "uiBoot"
        value: "+"
      - id: "videoAcceleration"
        value: "+"
  - categoryName: "Misc"
    features:
      - id: "anboxPatches"
        value: "+"
      - id: "apparmorPatches"
        value: "+"
      - id: "batteryPercentage"
        value: "+"
      - id: "offlineCharging"
        value: "+"
      - id: "onlineCharging"
        value: "+"
      - id: "recoveryImage"
        value: "+"
      - id: "factoryReset"
        value: "+"
      - id: "rtcTime"
        value: "+"
      - id: "sdCard"
        value: "+"
      - id: "shutdown"
        value: "+"
      - id: "wirelessCharging"
        value: "x"
      - id: "wirelessExternalMonitor"
        value: "-"
      - id: "waydroid"
        value: "+"
  - categoryName: "Network"
    features:
      - id: "bluetooth"
        value: "+"
      - id: "flightMode"
        value: "+"
      - id: "hotspot"
        value: "+"
      - id: "nfc"
        value: "x"
      - id: "wifi"
        value: "+"
  - categoryName: "Sensors"
    features:
      - id: "autoBrightness"
        value: "+"
      - id: "fingerprint"
        value: "+-"
      - id: "gps"
        value: "+"
      - id: "proximity"
        value: "+"
      - id: "rotation"
        value: "+"
      - id: "touchscreen"
        value: "+"
  - categoryName: "Sound"
    features:
      - id: "earphones"
        value: "+"
      - id: "loudspeaker"
        value: "+"
      - id: "microphone"
        value: "+"
      - id: "volumeControl"
        value: "+"
  - categoryName: "USB"
    features:
      - id: "mtp"
        value: "-"
      - id: "adb"
        value: "-"
      - id: "wiredExternalMonitor"
        value: "x"
deviceInfo:
  - id: "cpu"
    value: "Hexa-core 64-bit"
  - id: "arch"
    value: "arm64"
  - id: "chipset"
    value: "Qualcomm MSM8956 Snapdragon 650"
  - id: "gpu"
    value: "Qualcomm Adreno 510"
  - id: "rom"
    value: "16/32GB"
  - id: "ram"
    value: "3GB"
  - id: "android"
    value: "Android 6.0.1"
  - id: "battery"
    value: "4000 mAh"
  - id: "display"
    value: "1080x1920 pixels, 5.5 in"
  - id: "rearCamera"
    value: "16 MP"
  - id: "frontCamera"
    value: "5 MP"
  - id: "dimensions"
    value: "150 x 76 x 8.7 mm (5.91 x 2.99 x 0.34 in)"
  - id: "weight"
    value: "164 g (5.78 oz)"
contributors:
  - name: "mathew-dennis"
    forum: "https://github.com/mathew-dennis"
    photo: "https://gitlab.com/uploads/-/system/user/avatar/8316571/avatar.png?width=400"
communityHelp:
  - name: "Telegram"
    link: "https://t.me/utkenzo"
    icon: "telegram"
externalLinks:
  - name: "Telegram - @ubports"
    link: "https://t.me/joinchat/ubports"
    icon: "telegram"
  - name: "Source"
    link: "https://github.com/mathew-dennis"
    icon: "github"
---

### Known limitations

##### Sound

sound won't come up on every boot in such cases do a force reboot ,it should work from next boot.

##### Bluetooth

everything except file transfer works.

##### Camera

camera preview works but cant save picture and video.

#### More Details and status

Can be found [in this xda post](https://forum.xda-developers.com/t/rom-ubuntu-touch-halium-9-with-waydroid-10-support.4351735/)

### Installation

The port is not ready to use the installer, yet :)

#### If u prefer a flashable zip

Please download,

- [flashable zip](https://androidfilehost.com/?fid=17825722713688261051)
- [halium-boot.img](https://www.androidfilehost.com/?fid=7161016148664827562)

To install please reboot to twrp or orange fox recovery and flash halium-boot and the above zip file

#### If u prefer the standerd halium-install method

##### Fetch files and tools

Download and install the following tools:

- [Halium-install](https://gitlab.com/JBBgameich/halium-install/)

Download the following files:

- [halium-boot.img](https://www.androidfilehost.com/?fid=7161016148664827562)
- [system.img](https://androidfilehost.com/?fid=7161016148664801100)
- [ubports rootfs](https://ci.ubports.com/job/xenial-hybris-android9-rootfs-arm64/)

##### Unlock OEM installation

In Android activate developper mode and use MI unlock tool to do an oem unlock (skip this step if you are on a custom rom)

If this step is not done you willnot be able to flash hlium-boot.

##### Install TWRP or Orange fox recovery

Install the TWRP recovery as instructed in its [documentation](https://twrp.me/xiaomi/xiaomiredminote3.html).
or install Orange fox recovery from[here](https://orangefox.download/device/kenzo)

##### Reboot to recovery and format data

1. From the computer enter `adb reboot recovery`.
2. On your phone go to `wipe`->`Format Data` then enter `yes` as instructed.
3. From the PC Flash `system.img` using the halium-install script `./halium-install -p ut ubuntu-touch-hybris-xenial-arm64-rootfs.tar.gz system.img`.
4. Flash `halium-boot` from pc using fastboot ` fastboot flash boot halium-boot.img`.
5. Wait, after the reboot, you should have Ubuntu Touch Running on your Kenzo :-)
6. please reboot once more after initial setup is done .
