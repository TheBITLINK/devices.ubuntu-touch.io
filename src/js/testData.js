module.exports = {
  run: beforeElaboration
};

let Validator = require("./dataValidator.js");
const rules = require("../../data/validationRules.js")();

const path = require("path");

const ansiCodes = {
  yellowFG: "\x1b[33m",
  redFG: "\x1b[31m",
  greenFG: "\x1b[32m",
  reset: "\x1b[0m"
};

function beforeElaboration(device) {
  let ciDiff = [];

  try {
    ciDiff = process.env.CI_FILES_CHANGED.split("\n").map((e) => {
      return path.basename(e, device.fileInfo.extension);
    });
  } catch (e) {}

  if (
    !process.argv.includes("test-changes") ||
    ciDiff.includes(device.fileInfo.name)
  ) {
    let validation = new Validator(device, rules);
    validation.passes();
    let notValid = validation.errors();

    console.log(
      notValid.length
        ? ansiCodes.yellowFG + "%s" + ansiCodes.reset
        : ansiCodes.greenFG + "%s" + ansiCodes.reset,
      "\n[" + device.fileInfo.name + "] " + device.name
    );

    notValid.forEach((err) => console.log(err));
    device.notValid = notValid;
  }
}
